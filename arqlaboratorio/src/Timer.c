/*--------------------------------------------------------------------------*/

#include "Timer.h"
#include "cortexm/ExceptionHandlers.h"

/*--------------------------------------------------------------------------*/

#if defined(USE_HAL_DRIVER)
void HAL_IncTick(void);
#endif

// Forward declarations.

void TimingDelay_Decrement(void);

/*--------------------------------------------------------------------------*/

volatile timer_ticks_t timingDelay;

/*------------------------- Configuracion del tiempo -------------------------*/

void SysTick_Configure(void)
{
  SysTick_Config (SystemCoreClock / TIMER_FREQUENCY_HZ);
}

/*--------------------------------------------------------------------------*/

void Delay(uint32_t nTime)
{
	timingDelay = nTime;
	while(timingDelay != 0);
}

/*--------------------------------------------------------------------------*/

void TimingDelay_Decrement(void)
{
	if(timingDelay != 0x00)
	{
		timingDelay--;
	}
}

/*--------------------------------------------------------------------------*/

void SysTick_Handler(void)
{
#if defined(USE_HAL_DRIVER)
	HAL_IncTick();
#endif
	TimingDelay_Decrement();
}

/*--------------------------------------------------------------------------*/

