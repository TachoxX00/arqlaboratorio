/*----------------------- LIBRERIAS ------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include "diag/trace.h"
#include "Timer.h"
#include "BlinkLed.h"

/*------------------------ PRAGMA --------------------------------------------*/

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

/*------------------------- MAIN ---------------------------------------------*/

void main()
{
	//Inicializacion de LEDs.

	int leds[3];
	  leds[0]=12;
	  leds[1]=13;
	  leds[2]=14;
	  leds[3]=15;

	//Inicializacion funcion configuracion de tiempo.

	SysTick_Configure();

	//Inicializacion funcion configuracion de LEDs.

	GPIO_Configure();

  while (1)
  {

	  //Enviamos al metodo el numero de LED.
	  indicarLED(leds[0]);

	  	  //Encendemos el LED.
	  	  encenderLED();

	  	  //Se ejecuta el tiempo de espera.
	  	  Delay(300);

	  	  //Apagamos el LED.
	  	  apagarLED();

	  	  //Se ejecuta el tiempo de espera.
	  	  Delay(300);

	  	  //Repetimos los pasos indicados, con otro led.
	  	  indicarLED(leds[1]);
	  	  encenderLED();
	  	  Delay(300);
	  	  apagarLED();
	  	  Delay(300);

	  	  //Repetimos los pasos indicados, con otro led.
	  	  indicarLED(leds[2]);
	  	  encenderLED();
	  	  Delay(300);
	  	  apagarLED();
	  	  Delay(300);

	  	  //Repetimos los pasos indicados, con otro led.
	  	  indicarLED(leds[3]);
	  	  encenderLED();
	  	  Delay(300);
	  	  apagarLED();
	  	  Delay(300);



  }
}

/*---------------------------- PRAGMA ----------------------------------------*/

#pragma GCC diagnostic pop

/*--------------------------------------------------------------------------*/
