/*--------------------- Libreria Blinked.h -----------------------------------*/

#include "BlinkLed.h"

//Declaramos la variable LED, para el control de los mismos.
int LED = 0;

/*------------------ Configuracion de LED GPIO -------------------------------*/

void GPIO_Configure(void)
{

  // Enable GPIO Peripheral clock
  RCC->AHB1ENR |= BLINK_RCC_MASKx(BLINK_PORT_NUMBER);

  GPIO_InitTypeDef GPIO_InitStructure;

  // Configure pin in output push/pull mode
  GPIO_InitStructure.Pin = GPIO_PIN_12;
  GPIO_InitStructure.Pin = GPIO_PIN_13;
  GPIO_InitStructure.Pin = GPIO_PIN_14;
  GPIO_InitStructure.Pin = GPIO_PIN_15;
  GPIO_InitStructure.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStructure.Speed = GPIO_SPEED_FAST;
  GPIO_InitStructure.Pull = GPIO_PULLUP;
  HAL_GPIO_Init(BLINK_GPIOx(BLINK_PORT_NUMBER), &GPIO_InitStructure);

  // Start with led turned off
  blink_led_off();
}

/*------------------------- Encendemos el LED --------------------------------*/

void encenderLED()
{
	#if (BLINK_ACTIVE_LOW)
	HAL_GPIO_WritePin(BLINK_GPIOx(BLINK_PORT_NUMBER),
	BLINK_PIN_MASK(LED), GPIO_PIN_RESET);
	#else
	HAL_GPIO_WritePin(BLINK_GPIOx(BLINK_PORT_NUMBER),
	BLINK_PIN_MASK(LED), GPIO_PIN_SET);
	#endif
}

/*-------------------------- Apagamos el LED ---------------------------------*/

void apagarLED()
{
	#if (BLINK_ACTIVE_LOW)
	HAL_GPIO_WritePin(BLINK_GPIOx(BLINK_PORT_NUMBER),
    BLINK_PIN_MASK(LED), GPIO_PIN_SET);
	#else
	HAL_GPIO_WritePin(BLINK_GPIOx(BLINK_PORT_NUMBER),
	BLINK_PIN_MASK(LED), GPIO_PIN_RESET);
	#endif
}

/*--------------------- Indicamos el numero de LED ---------------------------*/

void indicarLED(int LEDx)
{
	LED = LEDx;
}

/*----------------------------------------------------------------------------*/
